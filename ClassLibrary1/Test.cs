﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{

    public class Test
    {

        [TestCase("Anastasiia", 0, 1)]
        [TestCase("Bogdan", 1, 1)]
        [TestCase("Chub", 15, 2)]
        [TestCase("72", 16, 3)]
        [TestCase("10", 9, 0)]

        public void TestingDataInPerson(string expRes, int rowsNum, int columnsNum)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");

            DataTable result = sqlConnector.Execute
                ("SELECT * FROM PERSON;");
            Assert.AreEqual(expRes, result.Rows[rowsNum].ItemArray[columnsNum].
                ToString());

        }

        [TestCase("Oksana", 0, 0)]
        [TestCase("Katya", 1, 0)]
        [TestCase("Leonid", 2, 0)]
        [TestCase("Tanya", 3, 0)]
        [TestCase("Vitaliy", 6, 0)]

        public void SelectIDWithPticeMore5000(string expRes, int rowsNum, int columnsNum)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");

            DataTable result = sqlConnector.Execute
                ("SELECT FirstName FROM PERSON WHERE ID IN (SELECT ID FROM ORDERS WHERE TotalPrice >=5000 );");
            Assert.AreEqual(expRes, result.Rows[rowsNum].ItemArray[columnsNum].
                ToString());

        }

        [TestCase("177", 0, 0)]
        [TestCase("1432", 1, 0)]
        [TestCase("1868", 2, 0)]
        [TestCase("2593", 3, 0)]
        [TestCase("2906", 4, 0)]
        [TestCase("3284", 5, 0)]
        [TestCase("6231", 6, 0)]
        [TestCase("177", 7, 0)]
        [TestCase("69", 8, 0)]
        
        public void SelectTotalPriceInCustomersTo30Years(string expRes, int rowsNum, int columnsNum)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");

            DataTable result = sqlConnector.Execute
                ("SELECT TotalPrice FROM ORDERS WHERE ID IN (SELECT ID FROM PERSON WHERE Age <=30 );");
            Assert.AreEqual(expRes, result.Rows[rowsNum].ItemArray[columnsNum].
                ToString());

        }

        [TestCase("ID", "Person", "24")]
        [TestCase("ID", "Orders", "26")]
        public void SELECTCountIdInTables(string ID, string nameOfTable, string expectedResult)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");
            DataTable responce = sqlConnector.Execute
                ($"SELECT COUNT ({ID}) FROM {nameOfTable};");
            Assert.AreEqual(expectedResult, responce.Rows[0].ItemArray[0].ToString());
        }


        [TestCase("Yaroslav", 0, 0)]

        public void SelectFromId(string expRes, int rowsNum, int columnsNum)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");

            DataTable result = sqlConnector.Execute
                ("SELECT FirstName FROM PERSON WHERE ID=11; ");
            Assert.AreEqual(expRes, result.Rows[rowsNum].ItemArray[columnsNum].
                ToString());

        }

        [TestCase("19", 0, 0)]
        [TestCase("21", 1, 0)]
        [TestCase("30", 2, 0)]
        [TestCase("20", 3, 0)]
        [TestCase("33", 10, 0)]
        public void SelectFromOrdersByTotalPriceASC(string expRes, int rowsNum, int columnsNum)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");

            DataTable result = sqlConnector.Execute
                ("SELECT ID FROM ORDERS WHERE ID>=1 ORDER BY TotalPrice ASC; ");
            Assert.AreEqual(expRes, result.Rows[rowsNum].ItemArray[columnsNum].
                ToString());

        }

        [TestCase("18", 0, 0)]
        [TestCase("17", 1, 0)]
        [TestCase("16", 2, 0)]
        [TestCase("15", 3, 0)]
        [TestCase("8", 10, 0)]
        public void SelectFromOrdersByTotalPriceDESC(string expRes, int rowsNum, int columnsNum)
        {
            SqlConnector.ConnectToCatalog("TestBase");
            SqlConnector sqlConnector = new SqlConnector("sa", "11111");

            DataTable result = sqlConnector.Execute
                ("SELECT ID FROM ORDERS WHERE ID>=1 ORDER BY TotalPrice DESC; ");
            Assert.AreEqual(expRes, result.Rows[rowsNum].ItemArray[columnsNum].
                ToString());

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace ClassLibrary1
{
    public class SqlConnector
    {
        private readonly SqlCredential _credential;
        static public string _connectionString;
        public SqlConnector(string login, string password)
        {
            var credential = new SecureString();
            for (var i = 0; i < password.Length; i++)
                credential.InsertAt(i, password[i]);
            credential.MakeReadOnly();
            _credential = new SqlCredential(login, credential);
        }


        public static void ConnectToCatalog(string catalogName)
        {
            _connectionString = "Data Source=DESKTOP-G0MIPMT;" +
                $"Initial Catalog={catalogName};";
        }


        public DataTable Execute(string sqlRequest)
        {
            var dataSet = new DataSet();
            using (var sqlConnection = new SqlConnection(_connectionString, _credential))
            {
                sqlConnection.Open();
                using (var sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;
                    var adapter = new SqlDataAdapter(sqlCommand);
                    adapter.Fill(dataSet);
                }
            }
            return dataSet.Tables[0];
        }

    }
}
